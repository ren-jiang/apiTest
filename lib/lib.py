import requests


# def printResponse(method, url, cookies, headers, params):
#     request_method = getattr(requests, method.lower())
#     response = request_method(url, cookies=cookies, headers=headers, params=params)
#     response = response.json()
#     return response


class API:

    # 打印响应
    def printRespones(self, response):
        print(response.status_code, end="")
        print(response.content.decode("utf8"))

    # 管理员登录
    def adminLogin(self, username="byhy", password=88888888):
        self.session = requests.Session()
        data = {
            "username": username,
            "password": password
        }
        url = "http://127.0.0.1/api/mgr/signin"
        response = self.session.post(url, data=data)
        self.printRespones(response)
        return response

    # 列出1页客户
    def showCustomer(self, action="list_customer", pagesize=5, pagenum=1, keywords=""):
        params = {
            "action": action,
            "pagesize": pagesize,
            "pagenum": pagenum,
            "keywords": keywords
        }
        url = "http://127.0.0.1/api/mgr/customers"
        response = self.session.get(url, params=params)
        self.printRespones(response)
        return response

    # 列出所有客户
    def showCustomerAll(self, action="list_customer", pagesize=999, pagenum=1, keywords=""):
        params = {
            "action": action,
            "pagesize": pagesize,
            "pagenum": pagenum,
            "keywords": keywords
        }
        url = "http://127.0.0.1/api/mgr/customers"
        response = self.session.get(url, params=params)
        self.printRespones(response)
        return response

    # 添加一个客户
    def addCustomer(self, action="add_customer", name="武汉市桥西医院", phonenumber="13345679934",
                    address="武汉市桥西医院北路"):
        url = "http://127.0.0.1/api/mgr/customers"
        response = self.session.post(url, json={
            "action": action,
            "data": {
                "name": name,
                "phonenumber": phonenumber,
                "address": address
            }
        })
        self.printRespones(response)
        return response

    # 添加多个客户
    def addCustomers(slef, number):
        for i in range(number):
            slef.addCustomer("add_customer", f"武汉市桥西医院{i + 1}", f"1334567993{i + 1}",
                             f"武汉市桥西医院北路{i + 1}")
        responese = slef.showCustomerAll()
        return responese

    # 修改一个客户
    def editCustomer(self, id, action="modify_customer", name="武汉市桥西医院", phonenumber="13345679934",
                     address="武汉市桥西医院北路"):
        url = "http://127.0.0.1/api/mgr/customers"
        response = self.session.put(url, json={
            "action": action,
            "id": id,
            "newdata": {
                "name": name,
                "phonenumber": phonenumber,
                "address": address
            }
        })
        self.printRespones(response)
        return response

    # 删除一个客户
    def delCustomer(self, id, action="del_customer"):
        url = "http://127.0.0.1/api/mgr/customers"
        response = self.session.delete(url, json={
            "action": action,
            "id": id
        })
        self.printRespones(response)
        return response

    # 删除所有客户
    def delCustomerAll(self):
        response = self.showCustomerAll()
        listResponse = response.json()["retlist"]
        for customer in listResponse:
            self.delCustomer(customer["id"])


api = API()
