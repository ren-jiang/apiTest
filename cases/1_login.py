from lib.lib import *
from hytest import *


class c1:
    name = "登录-api-1"

    def teststeps(self):
        response = api.adminLogin(username=None)

        INFO(response.json())

        expected = {"ret": 1, "msg": "用户名或者密码错误"}

        CHECK_POINT("返回一致", response.json() == expected)


class c2:
    name = "登录-api-2"

    def teststeps(self):
        response = api.adminLogin(username=None, password=None)

        INFO(response.json())

        expected = {"ret": 1, "msg": "用户名或者密码错误"}

        CHECK_POINT("返回一致", response.json() == expected)


class c3:
    name = "登录-api-3"

    def teststeps(self):
        response = api.adminLogin(password=66666666)

        INFO(response.json())

        expected = {"ret": 1, "msg": "用户名或者密码错误"}

        CHECK_POINT("返回一致", response.json() == expected)


class c4:
    name = "登录-api-4"

    def teststeps(self):
        response = api.adminLogin(username="renjiang")

        INFO(response.json())

        expected = {"ret": 1, "msg": "用户名或者密码错误"}

        CHECK_POINT("返回一致", response.json() == expected)
