from lib.lib import *
from hytest import *


class c1:
    name = "客户-api-01"

    def setup(self):
        api.adminLogin()
        api.delCustomerAll()

    def teardown(self):
        api.delCustomerAll()

    def teststeps(self):
        STEP(1, "增加客户")

        response = api.addCustomer()

        ID = response.json()["id"]

        INFO(f"增加客户生成的id：{ID}")

        STEP(2, "修改客户")

        api.editCustomer(ID, "modify_customer", "北京市协和医院", "15185877883", "北京市大木仓胡同")

        STEP(3, "查看客户")

        actual = api.showCustomer().json()

        INFO(f"响应报文：{actual}")

        CHECK_POINT("id一致且状态码正确", ID == actual["retlist"][0]["id"] and actual["ret"] == 0)


class c2:
    name = "客户-api-2"

    def setup(self):
        api.adminLogin()
        api.delCustomerAll()

    def teardown(self):
        api.delCustomerAll()

    def teststeps(self):
        response = api.delCustomer(777).json()

        INFO(response)

        expected = {"ret": 1, "msg": "客户ID不存在"}

        CHECK_POINT("返回一致", expected == response)
